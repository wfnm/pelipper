package main

import (
	"context"
	"database/sql"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"net"
	"net/http"
	"net/url"
	"reflect"
	"regexp"
	"runtime"
	"sort"
	"strings"
	"sync"
	"text/template"
	"time"

	"github.com/bwmarrin/discordgo"
	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"github.com/kelseyhightower/envconfig"
	"github.com/lib/pq"
	"golang.org/x/oauth2"
)

const sourceURL = "https://gitlab.com/wooloofarm/pelipper"

type Config struct {
	Token          string
	Dsn            string
	ManagedGuildID string
	MailGuildID    string

	NewChatLogChannelID string
	AdminChannelIDs     []string

	DiscordClientID     string
	DiscordClientSecret string
	ListenAddr          string `default:"localhost:9070"`
	BaseURL             string

	RenotifyAfter time.Duration `default:"10m"`

	HistoryReplayLimit int `default:"10"`
	IgnoreRegexp       string
}

func fullUsername(user *discordgo.User) string {
	return user.Username + "#" + user.Discriminator
}

type Bot struct {
	config             *Config
	db                 *sql.DB
	session            *discordgo.Session
	myID               string
	adminChannelIDsSet map[string]bool

	mailChannelIDs   []string
	mailChannelIDsMu sync.Mutex

	ignoreRegexp *regexp.Regexp

	guildName   string
	guildNameMu sync.RWMutex

	lis    net.Listener
	router *mux.Router
}

func (b *Bot) setGuildName(guildName string) {
	b.guildNameMu.Lock()
	defer b.guildNameMu.Unlock()
	b.guildName = guildName
}

func (b *Bot) getGuildName() string {
	b.guildNameMu.RLock()
	defer b.guildNameMu.RUnlock()
	return b.guildName
}

func (b *Bot) makeDiscordOauth2Config() *oauth2.Config {
	return &oauth2.Config{
		ClientID:     b.config.DiscordClientID,
		ClientSecret: b.config.DiscordClientSecret,
		Scopes:       []string{"identify"},
		RedirectURL:  b.config.BaseURL + "/auth",
		Endpoint: oauth2.Endpoint{
			TokenURL: "https://discord.com/api/oauth2/token",
			AuthURL:  "https://discord.com/api/oauth2/authorize",
		},
	}
}

func (b *Bot) handleLog(w http.ResponseWriter, r *http.Request, params map[string]string) error {
	userID := params["userID"]

	conf := b.makeDiscordOauth2Config()
	authURL := conf.AuthCodeURL(url.Values{
		"user_id": []string{userID},
	}.Encode())

	code := r.URL.Query().Get("code")
	if code == "" {
		http.Redirect(w, r, authURL, http.StatusFound)
		return nil
	}

	token, err := conf.Exchange(r.Context(), code)
	if err != nil {
		if retrieveErr := (&oauth2.RetrieveError{}); errors.As(err, &retrieveErr) && retrieveErr.Response.StatusCode == http.StatusBadRequest {
			http.Redirect(w, r, authURL, http.StatusFound)
			return nil
		}
		return err
	}

	client := conf.Client(r.Context(), token)

	rawResp, err := client.Get("https://discord.com/api/users/@me")
	if err != nil {
		return err
	}

	body, err := ioutil.ReadAll(rawResp.Body)
	if err != nil {
		return err
	}

	if rawResp.StatusCode != http.StatusOK {
		return fmt.Errorf("failed to login: %s", body)
	}

	var resp struct {
		ID string
	}
	if err := json.Unmarshal(body, &resp); err != nil {
		return err
	}

	// Only check if the user is a member of the mail guild.
	if _, err := b.session.GuildMember(b.config.MailGuildID, resp.ID); err != nil {
		return err
	}

	w.Header().Set("Content-Type", "text/html")
	fmt.Fprintf(w, "<!doctype html><html><head><meta charset=\"utf-8\"><title>%s</title></head><body>", userID)
	rows, err := b.db.QueryContext(r.Context(), `
		select forwarded_message_id, author_id, author_name, incoming, content[1]
		from logs
		where user_id = $1
		order by user_id, forwarded_message_id
	`, userID)
	if err != nil {
		return err
	}
	defer rows.Close()

	for rows.Next() {
		var forwardedMessageID string
		var authorID string
		var authorName string
		var incoming bool
		var content string
		if err := rows.Scan(&forwardedMessageID, &authorID, &authorName, &incoming, &content); err != nil {
			return err
		}

		if content == "" {
			continue
		}

		var sigil string
		if incoming {
			sigil = "→"
		} else {
			sigil = "←"
		}

		ts, err := discordgo.SnowflakeTimestamp(forwardedMessageID)
		if err != nil {
			ts = time.Time{}
		}

		fmt.Fprintf(w, "<p>%s <strong>%s (%s)</strong> @ <time datetime=\"%s\">%s</time><br>%s</p>", sigil, template.HTMLEscapeString(authorName), authorID, ts.UTC().Format(time.RFC3339), ts.UTC().Format(time.RFC3339), strings.ReplaceAll(template.HTMLEscapeString(content), "\n", "<br>"))
	}

	if err := rows.Err(); err != nil {
		return err
	}

	fmt.Fprintf(w, "</body></html>")

	return nil
}

var errNotFound = errors.New("not found")

func (b *Bot) handleAuth(w http.ResponseWriter, r *http.Request, _ map[string]string) error {
	params, err := url.ParseQuery(r.URL.Query().Get("state"))
	if err != nil {
		return err
	}

	http.Redirect(w, r, "/logs/"+params.Get("user_id")+"?code="+r.URL.Query().Get("code"), http.StatusFound)
	return nil
}

func (b *Bot) handle404(w http.ResponseWriter, r *http.Request, _ map[string]string) error {
	w.WriteHeader(http.StatusNotFound)
	w.Write([]byte("not found"))
	return nil
}

func (b *Bot) wrap(f func(w http.ResponseWriter, r *http.Request, params map[string]string) error) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		params := mux.Vars(r)
		if err := f(w, r, params); err != nil {
			if errors.Is(err, context.Canceled) {
				return
			}

			if errors.Is(err, errNotFound) {
				b.router.NotFoundHandler.ServeHTTP(w, r)
				return
			}

			w.WriteHeader(500)
			fmt.Fprintf(w, "An error has occurred.")
			log.Printf("ERROR IN HANDLER %s: %s", runtime.FuncForPC(reflect.ValueOf(f).Pointer()).Name(), err)
		}
	})
}

func newBot(c *Config) (*Bot, error) {
	session, err := discordgo.New(c.Token)
	if err != nil {
		return nil, err
	}

	session.StateEnabled = false
	session.SyncEvents = true

	db, err := sql.Open("postgres", c.Dsn)
	if err != nil {
		return nil, err
	}

	adminChannelIDsSet := map[string]bool{}
	for _, id := range c.AdminChannelIDs {
		adminChannelIDsSet[id] = true
	}

	c.Token = ""
	log.Printf("Config: %+v", c)

	me, err := session.User("@me")
	if err != nil {
		return nil, err
	}

	router := mux.NewRouter()
	lis, err := net.Listen("tcp", c.ListenAddr)
	if err != nil {
		return nil, fmt.Errorf("failed to listen: %w", err)
	}

	var ignoreRegexp *regexp.Regexp
	if c.IgnoreRegexp != "" {
		var err error
		ignoreRegexp, err = regexp.Compile(c.IgnoreRegexp)
		if err != nil {
			return nil, err
		}
	}

	log.Printf("Listening on %s", lis.Addr())

	b := &Bot{
		config:             c,
		session:            session,
		db:                 db,
		myID:               me.ID,
		adminChannelIDsSet: adminChannelIDsSet,

		ignoreRegexp: ignoreRegexp,

		router: router,
		lis:    lis,
	}

	router.Handle("/auth", b.wrap(b.handleAuth)).Methods("GET")
	router.Handle("/logs/{userID}", b.wrap(b.handleLog)).Methods("GET")
	router.NotFoundHandler = b.wrap(b.handle404)

	b.session.Identify.Intents = discordgo.MakeIntent(
		discordgo.IntentsGuilds |
			discordgo.IntentsGuildWebhooks |
			discordgo.IntentsGuildMessages |
			discordgo.IntentsGuildMessageReactions |
			discordgo.IntentsGuildMessageTyping |
			discordgo.IntentsDirectMessages |
			discordgo.IntentsDirectMessageTyping |
			discordgo.IntentsDirectMessageReactions)

	b.session.AddHandler(b.ready)
	b.session.AddHandler(b.guildCreate)
	b.session.AddHandler(b.guildUpdate)
	b.session.AddHandler(b.messageCreate)
	b.session.AddHandler(b.messageDelete)
	b.session.AddHandler(b.messageUpdate)
	b.session.AddHandler(b.typingStart)
	b.session.AddHandler(b.channelCreate)
	b.session.AddHandler(b.channelDelete)
	return b, nil
}

type user struct {
	id            string
	userChannelID string
	mailChannelID string
	webhook       *discordgo.Webhook
}

const webhookName = "Pelipper StaffMail"

func (b *Bot) ensureWebhook(ctx context.Context, u *user) error {
	if u.webhook != nil {
		return nil
	}

	// Try find the appropriate webhook.
	webhooks, err := b.session.ChannelWebhooks(u.mailChannelID)
	if err != nil {
		return err
	}

	for _, webhook := range webhooks {
		if webhook.Name == webhookName {
			u.webhook = webhook
			break
		}
	}

	// If we haven't found one, create a new one.
	if u.webhook != nil {
		return nil
	}

	webhook, err := b.session.WebhookCreate(u.mailChannelID, webhookName, "")
	if err != nil {
		return err
	}

	u.webhook = webhook
	return nil
}

func (b *Bot) sendMailChannelMessage(ctx context.Context, u *user, data *discordgo.WebhookParams) (*discordgo.Message, error) {
	if err := b.ensureWebhook(ctx, u); err != nil {
		return nil, err
	}
	return b.session.WebhookExecute(u.webhook.ID, u.webhook.Token, true, data)
}

func (b *Bot) ensureUserChannel(ctx context.Context, u *user) error {
	if u.userChannelID != "" {
		return nil
	}

	uc, err := b.session.UserChannelCreate(u.id)
	if err != nil {
		return err
	}

	u.userChannelID = uc.ID
	if _, err := b.db.ExecContext(ctx, `
		update users
		set user_channel_id = $2
		where id = $1
	`, u.id, u.userChannelID); err != nil {
		return err
	}

	return nil
}

func (b *Bot) sendUserChannelMessage(ctx context.Context, u *user, content string) (*discordgo.Message, error) {
	if err := b.ensureUserChannel(ctx, u); err != nil {
		return nil, err
	}
	return b.session.ChannelMessageSend(u.userChannelID, content)
}

func (b *Bot) userByIDOrCreate(ctx context.Context, id string, userChannelID string) (*user, bool, error) {
	tx, err := b.db.BeginTx(ctx, &sql.TxOptions{})
	if err != nil {
		return nil, false, err
	}
	defer tx.Rollback()

	u := &user{}
	if err := tx.QueryRowContext(ctx, `
		select id, user_channel_id, mail_channel_id
		from users
		where id = $1
		for update
	`, id).Scan(&u.id, &u.userChannelID, &u.mailChannelID); err != nil {
		if !errors.Is(err, sql.ErrNoRows) {
			return nil, false, err
		}

		u = &user{
			id: id,
		}
	}

	if userChannelID != "" {
		u.userChannelID = userChannelID
	}

	var isNew bool
	if u.mailChannelID == "" {
		isNew = true

		dgUser, err := b.session.User(id)
		if err != nil {
			return nil, false, err
		}

		var mc *discordgo.Channel
		for {
			var err error
			mc, err = b.session.GuildChannelCreateComplex(b.config.MailGuildID, discordgo.GuildChannelCreateData{
				Name:  dgUser.Username + "#" + dgUser.Discriminator,
				Type:  discordgo.ChannelTypeGuildText,
				Topic: fmt.Sprintf("<@%s> (%s#%s, ID: %s)\n%s/logs/%s", dgUser.ID, dgUser.Username, dgUser.Discriminator, dgUser.ID, b.config.BaseURL, dgUser.ID),
			})
			if err != nil {
				if rErr := (&discordgo.RESTError{}); errors.As(err, &rErr) {
					if rErr.Message.Code == 30013 /* Maximum number of guild channels reached (500) */ {
						if err := func() error {
							b.mailChannelIDsMu.Lock()
							defer b.mailChannelIDsMu.Unlock()

							// Delete the oldest channel we know about.
							channelID := b.mailChannelIDs[len(b.mailChannelIDs)-1]
							b.mailChannelIDs = b.mailChannelIDs[:len(b.mailChannelIDs)-1]

							if _, err := b.session.ChannelDelete(channelID); err != nil {
								return err
							}

							return nil
						}(); err != nil {
							return nil, false, err
						}
						continue
					}
				}
				return nil, false, err
			}
			break
		}

		u.mailChannelID = mc.ID

		func() {
			b.mailChannelIDsMu.Lock()
			defer b.mailChannelIDsMu.Unlock()
			b.mailChannelIDs = append(b.mailChannelIDs, u.mailChannelID)
		}()
	}

	if _, err := tx.ExecContext(ctx, `
		insert into users (id, user_channel_id, mail_channel_id)
		values ($1, $2, $3)
		on conflict (id) do update set
			user_channel_id = excluded.user_channel_id,
			mail_channel_id = excluded.mail_channel_id
	`, u.id, u.userChannelID, u.mailChannelID); err != nil {
		return nil, false, err
	}

	if isNew {
		// Proactively ensure the webhook.
		if err := b.ensureWebhook(ctx, u); err != nil {
			return nil, false, err
		}

		type historyEntry struct {
			authorID     string
			authorName   string
			authorAvatar string
			content      string
		}

		history := make([]historyEntry, 0, b.config.HistoryReplayLimit)

		if err := func() error {
			rows, err := tx.QueryContext(ctx, `
				select author_id, author_name, author_avatar, content[1]
				from logs
				where user_id = $1 and content[1] != ''
				order by user_id desc, original_message_id desc
				limit $2
			`, u.id, b.config.HistoryReplayLimit)
			if err != nil {
				return err
			}
			defer rows.Close()

			for rows.Next() {
				var entry historyEntry
				if err := rows.Scan(&entry.authorID, &entry.authorName, &entry.authorAvatar, &entry.content); err != nil {
					return err
				}
				history = append(history, entry)
			}

			if err := rows.Err(); err != nil {
				return err
			}

			return nil
		}(); err != nil {
			return nil, false, err
		}

		if len(history) > 0 {
			b.sendNotice(u.mailChannelID, fmt.Sprintf("Previous message history exists for this user: showing the last %d messages.", len(history)))

			// Reverse the history to present it in the correct order.
			for i, j := 0, len(history)-1; i < j; i, j = i+1, j-1 {
				history[i], history[j] = history[j], history[i]
			}

			for _, entry := range history {
				username := entry.authorName
				discriminator := "0000"
				if i := strings.IndexByte(username, '#'); i != -1 {
					discriminator = username[i+1:]
					username = username[:i]
				}

				var avatarURL string
				if entry.authorAvatar == "" {
					avatarURL = discordgo.EndpointDefaultUserAvatar(discriminator)
				} else if strings.HasPrefix(entry.authorAvatar, "a_") {
					avatarURL = discordgo.EndpointUserAvatarAnimated(entry.authorID, entry.authorAvatar)
				} else {
					avatarURL = discordgo.EndpointUserAvatar(entry.authorID, entry.authorAvatar)
				}

				if _, err := b.sendMailChannelMessage(ctx, u, &discordgo.WebhookParams{
					AvatarURL: avatarURL,
					Username:  username,
					Content:   entry.content,
				}); err != nil {
					b.sendError(u.mailChannelID, fmt.Sprintf("An error occurred and history could not be fully replayed:\n```\n%s\n```\n", err))
					break
				}
			}

			b.sendNotice(u.mailChannelID, fmt.Sprintf("All previous message history for this user: <%s>", b.logURL(u.id)))
		}
	}

	if err := tx.Commit(); err != nil {
		return nil, false, err
	}

	return u, isNew, nil
}

func (b *Bot) userByMailChannelID(ctx context.Context, mailChannelID string) (*user, error) {
	u := &user{}
	if err := b.db.QueryRowContext(ctx, `
		select id, user_channel_id, mail_channel_id
		from users
		where mail_channel_id = $1
		for update
	`, mailChannelID).Scan(&u.id, &u.userChannelID, &u.mailChannelID); err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return nil, nil
		}
		return nil, err
	}
	return u, nil
}

func (b *Bot) userByUserChannelID(ctx context.Context, userChannelID string) (*user, error) {
	u := &user{}
	if err := b.db.QueryRowContext(ctx, `
		select id, user_channel_id, mail_channel_id
		from users
		where user_channel_id = $1
		for update
	`, userChannelID).Scan(&u.id, &u.userChannelID, &u.mailChannelID); err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return nil, nil
		}
		return nil, err
	}
	return u, nil
}

func (b *Bot) guildCreate(_ *discordgo.Session, event *discordgo.GuildCreate) {
	ctx := context.Background()

	if event.Guild.ID == b.config.ManagedGuildID {
		b.setGuildName(event.Guild.Name)
		return
	}

	if event.Guild.ID != b.config.MailGuildID {
		return
	}

	tx, err := b.db.BeginTx(ctx, &sql.TxOptions{})
	if err != nil {
		log.Fatalf("Failed to begin transaction: %s", err)
	}
	defer tx.Rollback()

	channelIDs := make([]string, len(event.Guild.Channels))
	for i, channel := range event.Guild.Channels {
		channelIDs[i] = channel.ID
	}

	if _, err := tx.ExecContext(ctx, `
		update users
		set mail_channel_id = ''
		where mail_channel_id != all($1)
	`, pq.Array(channelIDs)); err != nil {
		log.Fatalf("Failed to expunge unknown channels: %s", err)
	}

	channels := event.Guild.Channels
	sort.Slice(channels, func(i int, j int) bool {
		return channels[i].Position < channels[j].Position
	})

	func() {
		b.mailChannelIDsMu.Lock()
		defer b.mailChannelIDsMu.Unlock()

		mailChannelIDs := make([]string, len(channels))

		for i, channel := range channels {
			var exists bool
			if err := tx.QueryRowContext(ctx, `
				select exists (select 1 from users where mail_channel_id = $1)
			`, channel.ID).Scan(&exists); err != nil {
				log.Fatalf("Failed to check for channel %s: %s", channel.ID, err)
			}

			if !exists {
				if _, err := b.session.ChannelDelete(channel.ID); err != nil {
					log.Fatalf("Failed to delete channel %s: %s", channel.ID, err)
				}
				continue
			}

			mailChannelIDs[i] = channel.ID
		}

		if err := b.reorderChannelsLocked(mailChannelIDs); err != nil {
			log.Fatalf("Failed to perform initial reordering: %s", err)
		}
	}()

	if err := tx.Commit(); err != nil {
		log.Fatalf("Failed to commit transaction: %s", err)
	}
}

func (b *Bot) reorderChannelsLocked(channelIDs []string) error {
	channels := make([]*discordgo.Channel, len(channelIDs))
	for i, id := range channelIDs {
		channels[i] = &discordgo.Channel{
			ID:       id,
			Position: i,
		}
	}

	if err := b.session.GuildChannelsReorder(b.config.MailGuildID, channels); err != nil {
		return err
	}

	b.mailChannelIDs = channelIDs
	return nil
}

func (b *Bot) bumpChannel(channelID string) error {
	b.mailChannelIDsMu.Lock()
	defer b.mailChannelIDsMu.Unlock()

	if b.mailChannelIDs[0] == channelID {
		// Channel is already at front.
		return nil
	}

	mailChannelIDs := make([]string, 0, cap(b.mailChannelIDs))
	mailChannelIDs = append(mailChannelIDs, channelID)

	for _, id := range b.mailChannelIDs {
		if id == channelID {
			continue
		}
		mailChannelIDs = append(mailChannelIDs, id)
	}

	return b.reorderChannelsLocked(mailChannelIDs)
}

func (b *Bot) channelCreate(_ *discordgo.Session, event *discordgo.ChannelCreate) {
	ctx := context.Background()

	if event.GuildID != b.config.MailGuildID {
		return
	}

	var exists bool
	if err := b.db.QueryRowContext(ctx, `
		select exists (select 1 from users where mail_channel_id = $1)
	`, event.Channel.ID).Scan(&exists); err != nil {
		log.Fatalf("Failed to check for channel %s: %s", event.Channel.ID, err)
	}

	if !exists {
		if _, err := b.session.ChannelDelete(event.Channel.ID); err != nil {
			log.Fatalf("Failed to delete channel %s: %s", event.Channel.ID, err)
		}
		return
	}
}

func (b *Bot) channelDelete(_ *discordgo.Session, event *discordgo.ChannelDelete) {
	ctx := context.Background()

	if event.GuildID != b.config.MailGuildID {
		return
	}

	func() {
		b.mailChannelIDsMu.Lock()
		defer b.mailChannelIDsMu.Unlock()

		var i = 0
		for j, channelID := range b.mailChannelIDs {
			if channelID == event.Channel.ID {
				i = j
				break
			}
		}

		b.mailChannelIDs = append(b.mailChannelIDs[:i], b.mailChannelIDs[i+1:]...)
	}()

	if _, err := b.db.ExecContext(ctx, `
		update users
		set mail_channel_id = ''
		where mail_channel_id = $1
	`, event.Channel.ID); err != nil {
		log.Printf("Failed to clear mail channel %s: %s", event.Channel.ID, err)
	}
}

func (b *Bot) guildUpdate(_ *discordgo.Session, event *discordgo.GuildUpdate) {
	if event.Guild.ID != b.config.ManagedGuildID {
		return
	}

	b.setGuildName(event.Guild.Name)
}

func (b *Bot) logURL(userID string) string {
	return fmt.Sprintf("%s/logs/%s", b.config.BaseURL, userID)
}

func (b *Bot) handleDMTyping(event *discordgo.TypingStart) {
	ctx := context.Background()
	u, err := b.userByUserChannelID(ctx, event.ChannelID)
	if err != nil {
		log.Printf("Failed to get user for user channel %s: %s", event.ChannelID, err)
		return
	}
	if u == nil {
		return
	}
	b.session.ChannelTyping(u.mailChannelID)
}

func (b *Bot) handleMailTyping(event *discordgo.TypingStart) {
	ctx := context.Background()
	u, err := b.userByMailChannelID(ctx, event.ChannelID)
	if err != nil {
		log.Printf("Failed to get user for mail channel %s: %s", event.ChannelID, err)
		return
	}
	if u == nil {
		return
	}
	b.session.ChannelTyping(u.userChannelID)
}

func (b *Bot) typingStart(_ *discordgo.Session, event *discordgo.TypingStart) {
	if event.UserID == b.myID {
		return
	}

	if event.GuildID == "" {
		b.handleDMTyping(event)
		return
	}

	if event.GuildID == b.config.MailGuildID {
		b.handleMailTyping(event)
		return
	}
}

func messageToString(msg *discordgo.Message) string {
	var buf strings.Builder
	buf.WriteString(msg.Content)
	if len(msg.Attachments) > 0 {
		buf.WriteRune('\n')
		for _, attachment := range msg.Attachments {
			buf.WriteString(attachment.URL)
			buf.WriteRune('\n')
		}
	}
	return buf.String()
}

type logEntry struct {
	userID             string
	originalMessageID  string
	forwardedMessageID string
	authorID           string
	authorName         string
	authorAvatar       string
	incoming           bool
	content            []string
}

func (b *Bot) readLog(ctx context.Context, userID string, originalMessageID string) (*logEntry, error) {
	le := &logEntry{}
	if err := b.db.QueryRowContext(ctx, `
		select user_id, original_message_id, forwarded_message_id, author_id, author_name, author_avatar, incoming, content
		from logs
		where user_id = $1 and original_message_id = $2
	`, userID, originalMessageID).Scan(&le.userID, &le.originalMessageID, &le.forwardedMessageID, &le.authorID, &le.authorName, &le.authorAvatar, &le.incoming, pq.Array(&le.content)); err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return nil, nil
		}
		return nil, err
	}
	return le, nil
}
func (b *Bot) writeLog(ctx context.Context, userID string, originalMessageID string, forwardedMessageID, authorID string, authorName string, authorAvatar string, incoming bool, content string) error {
	if _, err := b.db.ExecContext(ctx, `
		insert into logs (user_id, original_message_id, forwarded_message_id, author_id, author_name, author_avatar, incoming, content)
		values ($1, $2, $3, $4, $5, $6, $7, $8)
		on conflict (user_id, original_message_id) do update
		set author_id = excluded.author_id, author_name = excluded.author_name, author_avatar = excluded.author_avatar, content = excluded.content || logs.content
	`, userID, originalMessageID, forwardedMessageID, authorID, authorName, authorAvatar, incoming, pq.Array([]string{content})); err != nil {
		return err
	}
	return nil
}

func (b *Bot) sendError(channelID string, message string) error {
	if _, err := b.session.ChannelMessageSendComplex(channelID, &discordgo.MessageSend{
		Embed: &discordgo.MessageEmbed{
			Color:       0xff0000,
			Description: message,
		},
	}); err != nil {
		return err
	}
	return nil
}

func (b *Bot) sendNotice(channelID string, message string) error {
	if _, err := b.session.ChannelMessageSendComplex(channelID, &discordgo.MessageSend{
		Embed: &discordgo.MessageEmbed{
			Color:       0x0000ff,
			Description: message,
		},
	}); err != nil {
		return err
	}
	return nil
}

func (b *Bot) lastActiveTime(ctx context.Context, userID string) (time.Time, error) {
	var messageID string
	if err := b.db.QueryRowContext(ctx, `
		select original_message_id
		from logs
		where user_id = $1
		order by original_message_id desc
		limit 1
	`, userID).Scan(&messageID); err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return time.Time{}, nil
		}
		return time.Time{}, err
	}

	return discordgo.SnowflakeTimestamp(messageID)
}

func (b *Bot) handleDM(event *discordgo.MessageCreate) {
	ctx := context.Background()

	u, isNew, err := b.userByIDOrCreate(ctx, event.Author.ID, event.ChannelID)
	if err != nil {
		log.Printf("Failed to get user for user ID %s: %s", event.Author.ID, err)
		return
	}

	if !isNew {
		lastActiveTime, err := b.lastActiveTime(ctx, u.id)
		if err != nil {
			log.Printf("Failed to get last active time for user ID %s: %s", event.Author.ID, err)
		}
		isNew = time.Now().After(lastActiveTime.Add(b.config.RenotifyAfter))
	}

	if isNew {
		b.notifyNewMail(u, event.Author)
	}

	forwardedMessage, err := b.sendMailChannelMessage(ctx, u, &discordgo.WebhookParams{
		AvatarURL: event.Author.AvatarURL(""),
		Username:  event.Author.Username,
		Content:   messageToString(event.Message),
	})
	if err != nil {
		b.sendError(event.ChannelID, "An error occurred and your message could not be sent.")
		log.Printf("Failed to handle DM from %s: %s", event.Author.ID, err)
		return
	}

	if err := b.writeLog(ctx, u.id, event.Message.ID, forwardedMessage.ID, event.Author.ID, fullUsername(event.Author), event.Author.Avatar, true, forwardedMessage.Content); err != nil {
		log.Printf("Failed to write to incoming log for user ID %s: %s", u.id, err)
	}

	if err := b.bumpChannel(u.mailChannelID); err != nil {
		log.Printf("Failed to bump channel %s: %s", u.mailChannelID, err)
	}
}

func (b *Bot) handleMail(event *discordgo.MessageCreate) {
	ctx := context.Background()

	if b.ignoreRegexp != nil && b.ignoreRegexp.MatchString(event.Message.Content) {
		return
	}

	u, err := b.userByMailChannelID(ctx, event.ChannelID)
	if err != nil {
		log.Printf("Failed to get user for user ID %s: %s", u.id, err)
		return
	}

	forwardedMessage, err := b.sendUserChannelMessage(ctx, u, messageToString(event.Message))
	if err != nil {
		b.sendError(event.ChannelID, fmt.Sprintf("An error occurred and your message could not be sent.\n```\n%s\n```", err))
		log.Printf("Failed to handle outbound mail to %s: %s", u.id, err)
		return
	}

	if err := b.writeLog(ctx, u.id, event.Message.ID, forwardedMessage.ID, event.Author.ID, fullUsername(event.Author), event.Author.Avatar, false, forwardedMessage.Content); err != nil {
		log.Printf("Failed to write to outgoing log for user ID %s: %s", u.id, err)
	}

	if err := b.bumpChannel(u.mailChannelID); err != nil {
		log.Printf("Failed to bump channel %s: %s", u.mailChannelID, err)
	}
}

func (b *Bot) notifyNewMail(u *user, user *discordgo.User) error {
	if _, err := b.session.ChannelMessageSendComplex(b.config.NewChatLogChannelID, &discordgo.MessageSend{
		Embed: &discordgo.MessageEmbed{
			Color:       0x4e9e57,
			Title:       ":envelope: You've got mail!",
			Description: fmt.Sprintf("<@%s>", user.ID),
			Thumbnail: &discordgo.MessageEmbedThumbnail{
				URL: user.AvatarURL(""),
			},
			Fields: []*discordgo.MessageEmbedField{
				{
					Name:  "Channel",
					Value: fmt.Sprintf("<#%s>", u.mailChannelID),
				},
				{
					Name:  "Log URL",
					Value: b.logURL(u.id),
				},
			},
			Footer: &discordgo.MessageEmbedFooter{
				Text: fmt.Sprintf("%s#%s (ID: %s) | %s", user.Username, user.Discriminator, user.ID, sourceURL),
			},
		},
	}); err != nil {
		return err
	}
	return nil
}

func (b *Bot) handleAdmin(event *discordgo.MessageCreate) {
	ctx := context.Background()

	if parts := strings.SplitN(event.Message.Content, " ", 2); parts[0] == "!mail" {
		if len(parts) != 2 {
			b.sendError(event.ChannelID, "You must provide a user ID.")
			return
		}

		targetUserID := parts[1]

		u, isNew, err := b.userByIDOrCreate(ctx, targetUserID, "")
		if err != nil {
			b.sendError(event.ChannelID, fmt.Sprintf("Failed to create channel:\n```\n%s\n```", err))
			return
		}

		b.session.ChannelMessageSend(event.ChannelID, fmt.Sprintf("**Mail channel:** <#%s>", u.mailChannelID))

		if isNew {
			if err := b.bumpChannel(u.mailChannelID); err != nil {
				log.Printf("Failed to bump channel: %s", err)
			}
		}
	}
}

func (b *Bot) messageCreate(_ *discordgo.Session, event *discordgo.MessageCreate) {
	if event.Author.ID == b.myID || event.Message.WebhookID != "" {
		return
	}

	if event.GuildID == "" {
		b.handleDM(event)
		return
	}

	if b.adminChannelIDsSet[event.ChannelID] {
		b.handleAdmin(event)
		return
	}

	if event.GuildID == b.config.MailGuildID {
		b.handleMail(event)
		return
	}
}

func (b *Bot) handleDMUpdate(event *discordgo.MessageUpdate) {
	ctx := context.Background()

	if event.Message.Content == "" {
		return
	}

	u, err := b.userByUserChannelID(ctx, event.ChannelID)
	if err != nil {
		log.Printf("Failed to get user for user ID %s: %s", u.id, err)
		return
	}

	le, err := b.readLog(ctx, u.id, event.Message.ID)
	if err != nil {
		b.sendError(event.ChannelID, "An error occurred and your message could not be edited.")
		log.Printf("Failed to get DM log entry for user ID %s, message ID %s: %s", u.id, event.Message.ID, err)
		return
	}

	if le == nil {
		b.sendError(event.ChannelID, "An error occurred and your message could not be edited.")
		return
	}

	content := messageToString(event.Message)
	if u.mailChannelID != "" {
		um, err := b.sendMailChannelMessage(ctx, u, &discordgo.WebhookParams{
			AvatarURL: event.Author.AvatarURL(""),
			Username:  event.Author.Username + " (EDITED)",
			Content:   content,
		})
		if err != nil {
			b.sendError(event.ChannelID, "An error occurred and your message could not be edited.")
			log.Printf("Failed to handle DM edit from %s: %s", event.Author.ID, err)
			return
		}
		content = um.Content
	}

	if err := b.writeLog(ctx, u.id, event.Message.ID, le.forwardedMessageID, le.authorID, fullUsername(event.Author), event.Author.Avatar, le.incoming, content); err != nil {
		log.Printf("Failed to write new DM log entry update for user ID %s, message ID %s: %s", u.id, event.Message.ID, err)
	}
}

func (b *Bot) handleMailUpdate(event *discordgo.MessageUpdate) {
	ctx := context.Background()

	if event.Message.Content == "" {
		return
	}

	u, err := b.userByMailChannelID(ctx, event.ChannelID)
	if err != nil {
		log.Printf("Failed to get user for user ID %s: %s", u.id, err)
		return
	}

	le, err := b.readLog(ctx, u.id, event.Message.ID)
	if err != nil {
		b.sendError(event.ChannelID, fmt.Sprintf("An error occurred and your message could not be edited.\n```\n%s\n```", err))
		log.Printf("Failed to get mail log entry for user ID %s, message ID %s: %s", u.id, event.Message.ID, err)
		return
	}

	if le == nil {
		b.sendError(event.ChannelID, fmt.Sprintf("An error occurred and your message could not be edited.\n```\n%s\n```", "message not found"))
		return
	}

	um, err := b.session.ChannelMessageEdit(u.userChannelID, le.forwardedMessageID, messageToString(event.Message))
	if err != nil {
		b.sendError(event.ChannelID, fmt.Sprintf("An error occurred and your message could not be edited.\n```\n%s\n```", err))
		log.Printf("Failed to handle mail edit to %s: %s", event.Author.ID, err)
		return
	}

	if err := b.writeLog(ctx, u.id, event.Message.ID, le.forwardedMessageID, le.authorID, fullUsername(event.Author), event.Author.Avatar, le.incoming, um.Content); err != nil {
		log.Printf("Failed to write new mail log entry update for user ID %s, message ID %s: %s", u.id, event.Message.ID, err)
	}
}

func (b *Bot) messageUpdate(_ *discordgo.Session, event *discordgo.MessageUpdate) {
	if (event.Author != nil && event.Author.ID == b.myID) || event.Message.WebhookID != "" {
		return
	}

	if event.GuildID == "" {
		b.handleDMUpdate(event)
		return
	}

	if event.GuildID == b.config.MailGuildID {
		b.handleMailUpdate(event)
		return
	}
}

func (b *Bot) handleMailDelete(event *discordgo.MessageDelete) {
	ctx := context.Background()

	u, err := b.userByMailChannelID(ctx, event.ChannelID)
	if err != nil {
		log.Printf("Failed to get user for user ID %s: %s", u.id, err)
		return
	}

	le, err := b.readLog(ctx, u.id, event.Message.ID)
	if err != nil {
		b.sendError(event.ChannelID, "An error occurred and your message could not be deleted.")
		log.Printf("Failed to get mail log entry for user ID %s, message ID %s: %s", u.id, event.Message.ID, err)
		return
	}

	if le == nil {
		b.sendError(event.ChannelID, "An error occurred and your message could not be deleted.")
		return
	}

	if err := b.session.ChannelMessageDelete(u.userChannelID, le.forwardedMessageID); err != nil {
		b.sendError(event.ChannelID, "An error occurred and your message could not be deleted.")
		log.Printf("Failed to handle outbound mail delete to %s: %s", u.id, err)
		return
	}

	if err := b.writeLog(ctx, u.id, event.Message.ID, le.forwardedMessageID, le.authorID, le.authorName, le.authorAvatar, le.incoming, ""); err != nil {
		log.Printf("Failed to write new mail log entry delete for user ID %s, message ID %s: %s", u.id, event.Message.ID, err)
	}
}

func (b *Bot) messageDelete(_ *discordgo.Session, event *discordgo.MessageDelete) {
	if event.GuildID == "" {
		return
	}

	if event.GuildID == b.config.MailGuildID {
		b.handleMailDelete(event)
		return
	}
}

func (b *Bot) ready(_ *discordgo.Session, event *discordgo.Ready) {
	go b.session.UpdateStatus(0, "DM me to send staff mail!")
}

func (b *Bot) run() error {
	if err := b.session.Open(); err != nil {
		return err
	}

	var handler http.Handler = b.router
	handler = handlers.ProxyHeaders(handler)
	return (&http.Server{
		Handler: handler,
	}).Serve(b.lis)
}

func main() {
	var c Config
	if err := envconfig.Process("pelipper", &c); err != nil {
		log.Fatalf("Failed to parse config: %s", err)
	}

	bot, err := newBot(&c)
	if err != nil {
		log.Fatalf("failed to create discord client: %s", err)
	}

	if err := bot.run(); err != nil {
		log.Fatalf("failed to connect to discord: %s", err)
	}
}
