create table users (
    id text primary key,
    user_channel_id text not null,
    mail_channel_id text not null
);
create index on users (user_channel_id);
create index on users (mail_channel_id);

create table logs (
    user_id text not null,
    original_message_id text not null,
    forwarded_message_id text not null,
    author_id text not null,
    author_name text not null,
    author_avatar text not null,
    incoming boolean not null,
    content text[] not null,
    primary key (user_id, original_message_id),
    foreign key (user_id) references users(id)
);
